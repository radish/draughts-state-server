#!/usr/bin/env python3

from setuptools import setup, find_packages

from draughts_state_server import __version__

setup(
    name='draughts-state-server',
    version=__version__,
    url='https://gitlab.com/radish/draughts-state-server',
    install_requires=[
        'aiohttp',
        'msgpack-python',
        'ujson',
        'docker-py',
        'zeroconf',
        'pillow'
    ],
    extras_require={
       'uvloop': 'uvloop'
    },
    packages=find_packages(),
    scripts=['bin/dss', ]
)
