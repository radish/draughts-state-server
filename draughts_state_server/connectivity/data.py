import asyncio
import logging

import msgpack
from PIL import Image
from io import BytesIO

from draughts_state_server.connectivity.websockets import WebSocketsServer


class TCPServer:
    def __init__(self, event_loop: asyncio.BaseEventLoop,
                 ws_server: WebSocketsServer):
        self._loop = event_loop
        self._listen = asyncio.streams.start_server(
            self.on_connection, '127.0.0.1', 8888, loop=event_loop
        )
        self._server = None  # type: asyncio.base_events.Server
        self._ws_server = ws_server
        self._logger = logging.getLogger(
            'draughts-state-server.connectivity.data'
        )

    async def init_server(self):
        self._logger.info('TCP server starting...')
        self._server = await self._listen
        self._logger.info('TCP server is now listening')

    async def on_connection(self, reader: asyncio.StreamReader,
                            writer: asyncio.StreamWriter):
        addr = writer.get_extra_info('peername')
        self._logger.info('Client {} connected'.format(addr[0]))

        while True:
            try:
                length = int.from_bytes(await reader.readexactly(4), 'big')
                data = msgpack.unpackb(await reader.readexactly(length),
                                       encoding='utf-8', use_list=False)
            except asyncio.IncompleteReadError:
                break

            if data['_type'] == 0:
                # Packet with data
                self._logger.debug(
                    'Data received from client {}'.format(addr[0])
                )
                data_to_clients = {
                    'checker': data['payload'],
                    'status': None,
                    'cameraId': addr[0]
                }
                self._ws_server.send_data_to_all(data_to_clients)
            elif data['_type'] == 1:
                # Packet with image
                self._logger.debug(
                    'Video frame received from client {}'.format(addr[0])
                )
                payload = data['payload']
                image = Image.open(BytesIO(payload))
            elif data['_type'] == 3:
                # Awaiting empty board
                pass
            elif data['_type'] == 4:
                # Awaiting initial draughts setting
                pass
            elif data['_type'] == 5:
                # Awaiting game start
                pass

        self._logger.info('Client {} disconnected'.format(addr[0]))

    async def shutdown_server(self):
        self._server.close()
        await self._server.wait_closed()
