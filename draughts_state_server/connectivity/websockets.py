import asyncio
import logging
from typing import Any
from typing import List

import ujson
from aiohttp import MsgType
from aiohttp import web


class WebSocketsServer:
    def __init__(self, host: str, port: int, event_loop: asyncio.BaseEventLoop):
        self._host = host
        self._port = port
        self._loop = event_loop

        self._app = web.Application()
        self._app.router.add_route('GET', '/', self._websocket_handler)
        self._app['websockets'] = []  # type: List[web.WebSocketResponse]
        self._app.on_shutdown.append(self._close_ws_connections)

        self._handler = self._app.make_handler()
        self._server = None
        self._logger = logging.getLogger(
            'draughts-state-server.connectivity.websockets'
        )

    async def _websocket_handler(self, request):
        ws = web.WebSocketResponse()
        await ws.prepare(request)
        self._app['websockets'].append(ws)

        peername = request.transport.get_extra_info('peername')
        if peername is not None:
            client_ip, _ = peername
        else:
            client_ip = None

        self._logger.debug('New WebSocket connection from {}'.format(client_ip))

        async for msg in ws:
            if msg.tp == MsgType.text:
                self._logger.debug(
                    'WebSocket message received: {}'.format(msg.data)
                )
            elif msg.tp == MsgType.error:
                self._logger.debug(
                    'WebSocket connection closed with exception {}'.format(
                        ws.exception()
                    )
                )

        self._app['websockets'].remove(ws)
        self._logger.debug(
            'WebSocket connection with {} closed'.format(client_ip)
        )

        return ws

    async def _close_ws_connections(self, app):
        self._logger.debug('Closing WebSockets connections...')

        for ws in app['websockets']:
            await ws.close(code=1001, message='Server shutdown')

    async def init_server(self):
        self._logger.info('WebSockets server starting...')
        self._server = await self._loop.create_server(self._handler,
                                                      self._host, self._port)
        self._logger.info('WebSockets server is now listening')

    async def shutdown_server(self):
        self._logger.info('WebSockets server shutting down...')
        self._server.close()
        await self._server.wait_closed()
        await self._app.shutdown()
        await self._handler.finish_connections(60.0)
        await self._app.cleanup()

    def send_data_to_all(self, data: Any):
        packed_data = ujson.dumps(data)

        for ws in self._app['websockets']:
            self._logger.debug('Sending data...')
            ws.send_str(packed_data)
